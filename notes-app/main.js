new Vue({
  el: '#app',
  data: {
  	notes: [
      { text: 'This is a first note', favorite: false },
      { text: 'This is a second note', favorite: true },
    ]
  },
})
